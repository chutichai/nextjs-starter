import Link from 'next/link'
import Head from 'next/head'

const Post = ({ title, body, id }) => {
  return (
    <article>
      <h2>{title}</h2>
      <p>{body}</p>
      <Link href={`/blogs/${id}`}>
        <a>Read more...</a>
      </Link>
    </article>
  )
}

export default function IndexBlog({ postList }) {
  return (
    <main>
      <Head>
        <title>Home page</title>
      </Head>

      <h1>List of posts</h1>

      <section>
        {postList.map((post) => (
          <Post {...post} key={post.id} />
        ))}
      </section>
    </main>
  )
}

export async function getStaticProps() {
  // fetch list of posts
  const response = await fetch(
    'https://jsonplaceholder.typicode.com/posts?_page=1'
  )
  const postList = await response.json()
  return {
    props: {
      postList,
    },
  }
}